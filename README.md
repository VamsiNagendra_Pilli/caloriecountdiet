
# Group Member names
1.Vamsi Nagendra Pilli 2.Balamani Sandeep Linga 3.Sai Narasimha Kumara Raja Goda
# Application Information
The name of application is Fitness Tracker . The purpose of this application is to create functionality where users can create their fitness goals , track their fitness progress etc. By using this application the users can acheive their objective of becoming fit.
# Team members Contribution 
  Vamsi Nagendra Pilli -> Home , Goal, Database ,Database schema .            
  Balamani Sandeep Linga -> Signup , Sign up goal , Profile .          
  Sai Narasimha Kumara Raja Goda ->  Categories , Food , navigation .        
  
  The database used for this project is SQLLite Database . The databaseVersion used is 57 . Stetho a debug bridge for android applications
  is used as a dependency for the project. The device used for testing the project is Nexus 5x API 25 .